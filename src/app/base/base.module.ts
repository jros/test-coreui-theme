import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { XHRBackend, RequestOptions } from "@angular/http";

import { ApiXHRBackend } from "./http/api-xhr-backend";
import { ApiRequestOptions } from "./http/api-request-options";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: []
})
export class BaseModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: BaseModule,
            providers: [
                { provide: XHRBackend, useClass: ApiXHRBackend },
                { provide: RequestOptions, useClass: ApiRequestOptions }
            ]
        };
    }
}
