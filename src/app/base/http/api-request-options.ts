import { BaseRequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";

@Injectable()
export class ApiRequestOptions extends BaseRequestOptions {
    constructor() {
        super();
        this.headers.append("X-Requested-With", "XMLHttpRequest");
        this.withCredentials = true;
    }
}
