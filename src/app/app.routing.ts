import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Layouts
import { FullLayoutComponent } from "./layouts/full-layout.component";
import { LoginComponent } from "./security/login.component";
import { EmptyLayoutComponent } from "./layouts/empty-layout.component";

// Components

export const routes: Routes = [
    {
        path: "",
        redirectTo: "dashboard",
        pathMatch: "full",
    },
    {
        path: "",
        component: FullLayoutComponent,
        data: {
            title: "Inicio"
        },
        children: [
            {
                path: "dashboard",
                loadChildren: "./dashboard/dashboard.module#DashboardModule"
            },
        ]
    },
    {
        path: "",
        component: EmptyLayoutComponent,
        data: {
            title: "Entrar"
        },
        children: [
            {
                path: "login",
                component: LoginComponent
            },
        ]
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
