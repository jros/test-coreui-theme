import { Http, Headers, RequestOptionsArgs } from "@angular/http";
import { Injectable } from "@angular/core";
import "rxjs/add/operator/toPromise";

@Injectable()
export class AccountService {

    constructor(private http: Http) { }

    login(username: string, password: string): Promise<boolean> {

        return this.http.post("/Account/LoginAjax",
            {
                Email: username,
                Password: password
            }
        ).toPromise().then((res) => {
            const result = res.json() as boolean;
            if (result) {

                this.getValue();
            }
            return result;
        });
    }

    getValue() {
        return this.http.post("/Home/GetValue", null).toPromise()
            .then(res => {
                console.log(res);
            });
    }

}
