import { FormsModule } from "@angular/forms";
import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HttpModule } from "@angular/http";

import { LoginComponent } from "./login.component";
import { AccountService } from "./account.service";

@NgModule({
    imports: [
        CommonModule, FormsModule,
        HttpModule
    ],
    declarations: [LoginComponent],
    exports: [LoginComponent],
    providers: [AccountService]
})
export class SecurityModule {
    public static forRoot(): ModuleWithProviders {
        return {
            ngModule: SecurityModule,
            providers: [AccountService]
        };
    }
}
