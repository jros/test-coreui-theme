import { AccountService } from "./account.service";
import { Component, OnInit } from "@angular/core";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styles: [`
    .login-page {
        background: #303641;
    }
    .login-page img {
        height: 70px;
        width: auto;
    }
    .welcome-zone {
        padding-top: 5rem;
        padding-bottom: 5rem;
        background: #373e4a;
    }
    .login-page .card {       
        background: transparent;
    }
    .input-group-addon {
        background: #373e4a !important;
    }
    .form-control {
        background: #373e4a !important;
    }
    button {
        cursor: pointer;
    }
    button:disabled {
        cursor: not-allowed;
    }
  `]
})
export class LoginComponent implements OnInit {

    userName: string;
    password: string;

    constructor(private accountService: AccountService) { }

    ngOnInit() {
    }

    login() {
        this.accountService.login(this.userName, this.password);
    }

}
