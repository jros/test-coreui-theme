import { BaseModule } from "./base/base.module";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { HttpModule } from "@angular/http";

import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { TabsModule } from "ngx-bootstrap/tabs";

import { AppComponent } from "./app.component";
import { NAV_DROPDOWN_DIRECTIVES } from "./shared/nav-dropdown.directive";

// import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from "./shared/sidebar.directive";
import { AsideToggleDirective } from "./shared/aside.directive";
import { BreadcrumbsComponent } from "./shared/breadcrumb.component";

// Routing Module
import { AppRoutingModule } from "./app.routing";

// Layouts
import { FullLayoutComponent } from "./layouts/full-layout.component";
import { EmptyLayoutComponent } from "./layouts/empty-layout.component";
import { SecurityModule } from "./security/security.module";

@NgModule({
    imports: [
        BrowserModule, HttpModule,
        AppRoutingModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        SecurityModule.forRoot(),
        BaseModule.forRoot()
        // ChartsModule
    ],
    declarations: [
        AppComponent,
        FullLayoutComponent,
        NAV_DROPDOWN_DIRECTIVES,
        BreadcrumbsComponent,
        SIDEBAR_TOGGLE_DIRECTIVES,
        AsideToggleDirective,
        EmptyLayoutComponent
    ],
    providers: [{
        provide: LocationStrategy,
        useClass: PathLocationStrategy
    }],
    bootstrap: [AppComponent]
})
export class AppModule { }
